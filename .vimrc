" Include the pathogen plugin handler
execute pathogen#infect()

" Enable syntax highlighting
syntax enable
 
" Don't fail if blue colorscheme is missing
try
    colorscheme blue
catch
endtry

" Set the mapleader
let mapleader=" "

" Set the path
set path=~/Gitrepos/analytics-sql/tests,~/Gitrepos/analytics/vendor/analytics_setup/sql

" Make file/command completion useful
set wildmenu
set wildmode=list:longest

" Do not stay in vi compatibility mode
set nocompatible
 
" Do not wrap text. This is used for programming but I would change for normal writing
set nowrap
 
set copyindent
 
" Ignore case when searching
set ignorecase
 
" Don't ignore case if capital letters are used in search string 
set smartcase
 
" do not highlight search text
set nohlsearch
 
" Use spaces instead of tabs
set expandtab
 
" Make tabs work logically when backspacing over them and jumping to next tab
set smarttab

" Set tab == 4 spaces
set shiftwidth=4
set tabstop=4
 
set clipboard=unnamedplus,unnamed,autoselect
" Autoindent
set autoindent
 
" Allow buffers to exit out of view and in an unsaved state
set hidden
 
" History maintained and undo history
set history=1000
set undolevels=1000
 
" Toggle paste mode.  Paste mode informs vim that text should not be autoindented etc...
set pastetoggle=<F2>
 
" Makes backspace work as expected
set backspace=eol,start,indent
 
" Add a bit extra margin to the left
set foldcolumn=1
 
" Added ability to go to beginning and end of lines while in insert mode
inoremap <C-A> <C-O>^
inoremap <C-L> <C-O>$
 
" Remapping of ESC. I also added a left movement so you stay on the character when hitting escape.
inoremap qq <ESC>l
 
" This is here because I am using qq for escape.  I keep accidentally turning on macro recording.
nnoremap q <ESC>
 
" I like to add lines without going into insert mode. Pressing i once is easier than hitting esc several times
nnoremap o o<ESC>
nnoremap O O<ESC>
 
" Go up literal lines and not true lines.  So word wrap doesn't get you.
nnoremap j gj
nnoremap k gk
 
" Make capital Y behave in a similar manner to capital D
nnoremap Y y$

" Swap these leadings chars because ` is far more useful than ' and ' is easier to hit
nnoremap ' `
nnoremap ` '

" Make tab switching easier
map <C-K> :bn<CR>
map <C-J> :bp<CR>

" Toggle NerdTree
nnoremap <leader><space> :NERDTreeToggle<CR>

" Paste from yank buffer
nnoremap <leader>p "0p
nnoremap <leader>P "0P

" Paste from Mac Clipboard
nnoremap <leader>v :set paste<CR>:r !pbpaste<CR>
nnoremap <leader>c :call system("pbcopy", getreg("\""))<CR>

" function to toggle line numbering
function! NumberToggle()
    if(&relativenumber == 1)
       set foldcolumn=6
       set norelativenumber
    elseif(&number == 1)
       set foldcolumn=2
       set relativenumber
    else
       set foldcolumn=2
       set number
    endif
endfunc

" Make the tab in normal mode toggle the line numbers
nnoremap <C-i> :call NumberToggle()<cr>

" create left padding
set foldcolumn=6
highlight FoldColumn none 

" setup airline - show tabline with filename only. show it all the time.
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#fnamemod = ':t'

" These functions are specifically to turn on and turn off writers mode
function! WordProcessorModeOn()
    set spell spelllang=en_us
    set wrap
    set linebreak
    set textwidth=54
endfunc

function! WordProcessorModeOff()
    set nowrap ;
    set nolinebreak ;
    set textwidth=0 ;
endfunc

com! WP call WordProcessorModeOn()
com! NOWP call WordProcessorModeOff()

