PATH=$PATH:./

alias ll='/bin/ls -l -a -c'

cdfunc() {
    cd $@
    pwd
}

alias cd=cdfunc
cd ~